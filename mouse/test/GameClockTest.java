/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Graphics;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Waswas
 */
public class GameClockTest {
    
    GameClock instance = new GameClock(2);
    Graphics g;
    
    public GameClockTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new GameClock(2);
    }
    
    @After
    public void tearDown() {
        instance = new GameClock(2);
    }

    /**
     * Test of setLocation method, of class GameClock.
     */
    @Test
    public void testSetLocation() {
        System.out.println("setLocation");
        int x = 0;
        int y = 0;
        instance.setLocation(x, y);
    }

    /**
     * Test of setSize method, of class GameClock.
     */
    @Test
    public void testSetSize() {
        System.out.println("setSize");
        int s = 0;
        instance.setSize(s);
    }

    /**
     * Test of getSize method, of class GameClock.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        int expResult = 0;
        int result = instance.getSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTime method, of class GameClock.
     */
    @Test
    public void testSetTime_3args() {
        System.out.println("setTime");
        int sec = 0;
        int min = 0;
        int hour = 0;
        instance.setTime(sec, min, hour);
    }

    /**
     * Test of setTime method, of class GameClock.
     */
    @Test
    public void testSetTime_4args() {
        System.out.println("setTime");
        int frame = 0;
        int sec = 0;
        int min = 0;
        int hour = 0;
        instance.setTime(frame, sec, min, hour);
    }

    /**
     * Test of setSeconds method, of class GameClock.
     */
    @Test
    public void testSetSeconds() {
        System.out.println("setSeconds");
        int s = 0;
        instance.setSeconds(s);
    }

    /**
     * Test of getSeconds method, of class GameClock.
     */
    @Test
    public void testGetSeconds() {
        System.out.println("getSeconds");
        int expResult = 0;
        int result = instance.getSeconds();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMinutes method, of class GameClock.
     */
    @Test
    public void testSetMinutes() {
        System.out.println("setMinutes");
        int m = 0;
        instance.setMinutes(m);
    }

    /**
     * Test of getMinutes method, of class GameClock.
     */
    @Test
    public void testGetMinutes() {
        System.out.println("getMinutes");
        int expResult = 0;
        int result = instance.getMinutes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setHours method, of class GameClock.
     */
    @Test
    public void testSetHours() {
        System.out.println("setHours");
        int h = 0;
        instance.setHours(h);
    }

    /**
     * Test of getHours method, of class GameClock.
     */
    @Test
    public void testGetHours() {
        System.out.println("getHours");
        int expResult = 0;
        int result = instance.getHours();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTimer method, of class GameClock.
     */
    @Test
    public void testSetTimer() {
        System.out.println("setTimer");
        int t = 0;
        instance.setTimer(t);
    }

    /**
     * Test of getTimer method, of class GameClock.
     */
    @Test
    public void testGetTimer() {
        System.out.println("getTimer");
        int expResult = 0;
        int result = instance.getTimer();
        assertEquals(expResult, result);
    }

    /**
     * Test of drawTic method, of class GameClock.
     */
    @Test
    public void testDrawTic() {
        System.out.println("drawTic");
        double angle = 0.0;
        int radious = 0;
        int radious2 = 0;
        instance.drawTic(angle, radious, radious2, g);
    }

    /**
     * Test of drawHand method, of class GameClock.
     */
    @Test
    public void testDrawHand() {
        System.out.println("drawHand");
        double angle = 0.2;
        int radius = 5;
        instance.drawHand(angle, radius, g);
    }

    /**
     * Test of incrementSeconds method, of class GameClock.
     */
    @Test
    public void testIncrementSeconds() {
        System.out.println("incrementSeconds");
        instance.incrementSeconds();
    }

    /**
     * Test of increment method, of class GameClock.
     */
    @Test
    public void testIncrement_int() {
        System.out.println("increment");
        int f = 0;
        instance.increment(f);
    }

    /**
     * Test of increment method, of class GameClock.
     */
    @Test
    public void testIncrement_0args() {
        System.out.println("increment");
        instance.increment();
    }

    /**
     * Test of changeFramesPerSecond method, of class GameClock.
     */
    @Test
    public void testChangeFramesPerSecond() {
        System.out.println("changeFramesPerSecond");
        int framesPerSecond = 0;
        instance.changeFramesPerSecond(framesPerSecond);
    }
    
}
