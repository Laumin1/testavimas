/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Graphics;
import java.awt.Point;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Waswas
 */
public class BoardPanelTest {
    
    private RodentsRevengeGame game;
    BoardPanel instance = new BoardPanel(game);
    Level level = new Level(20,20);
    
    public BoardPanelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new BoardPanel(game);
        level = new Level(20,20);
    }
    
    @After
    public void tearDown() {
        instance = new BoardPanel(game);
        level = new Level(20,20);
    }

    /**
     * Test of clearBoard method, of class BoardPanel.
     */
    @Test
    public void testClearBoard() {
        System.out.println("clearBoard");
        instance.clearBoard();
    }

    /**
     * Test of boardFromLevel method, of class BoardPanel.
     */
    @Test
    public void testBoardFromLevel() {
        System.out.println("boardFromLevel");
        boolean expResult = false;
        boolean result = instance.boardFromLevel(level);
        System.out.println("atsakymas yra ==========="+result);
        assertEquals(expResult, result);
    }

    /**
     * Test of setTile method, of class BoardPanel.
     */
    @Test
    public void testSetTile_3args() {
        System.out.println("setTile");
        int x = 1;
        int y = 1;
        TileType type = TileType.Block;
        int expResult = instance.getNumberOfEmptySpaces()-1;
        instance.setTile(x, y, type);
        int result = instance.getNumberOfEmptySpaces();
        System.out.println();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberOfWallTiles method, of class BoardPanel.
     */
    @Test
    public void testGetNumberOfWallTiles() {
        System.out.println("getNumberOfWallTiles");
        int expResult = 0;
        int result = instance.getNumberOfWallTiles();
        assertEquals(expResult, result);

    }

    /**
     * Test of getNumberOfEmptySpaces method, of class BoardPanel.
     */
    @Test
    public void testGetNumberOfEmptySpaces() {
        System.out.println("getNumberOfEmptySpaces");
        int[] expResult = new int[100];
        for (int i=0;i<100;i++){
            expResult[i] = i;
        }
        int result = instance.getNumberOfEmptySpaces();
        for (int i=0;i<100;i++){
            assertEquals(expResult[i], result);
        }
    }

    /**
     * Test of getTile method, of class BoardPanel.
     */
    @Test
    public void testGetTile() {
        System.out.println("getTile");
        int x = 0;
        int y = 0;
        TileType expResult = TileType.Block;
        TileType result = instance.getTile(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberOfBlockTiles method, of class BoardPanel.
     */
    @Test
    public void testGetNumberOfBlockTiles() {
        System.out.println("getNumberOfBlockTiles");
        int expResult = 0;
        int result = instance.getNumberOfBlockTiles();
        assertEquals(expResult, result);
    }
    
}
