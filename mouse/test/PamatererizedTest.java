
import java.util.Arrays;
import java.util.Collection;
import junit.framework.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author Laurynas Minelga
 */
@RunWith(Parameterized.class)
public class PamatererizedTest {
    
    private int expectedResult;
    private int x;
    private int y;
    private TileType type;
    private RodentsRevengeGame game;
    BoardPanel panel;
    
    @Before
    public void setup(){
        panel = new BoardPanel(game);
    }
    
    public PamatererizedTest(int expectedResult, int x, int y, TileType type){
        this.expectedResult = expectedResult;
        this.x = x;
        this.y = y;
        this.type = type;
    }
    
    @Parameters
    public static Collection<Object[]> testData(){
        Object[][] data = new Object[][]{
            {528,10,10,TileType.Block},
            {528,20,20,TileType.Block},
            {528,15,15,TileType.Block}
        };
        return Arrays.asList(data);
    }
    
    @Test
    public void testAdd(){
        panel.setTile(x, y, type);
        int result = panel.getNumberOfEmptySpaces();
        assertEquals(expectedResult, result);
    }
    
}
