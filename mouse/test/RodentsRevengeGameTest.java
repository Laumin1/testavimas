/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Point;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Waswas
 */
public class RodentsRevengeGameTest {
    
    public RodentsRevengeGameTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of pause method, of class RodentsRevengeGame.
     */
    @Test
    public void testPause() {
        System.out.println("pause");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        instance.pause();
    }

    /**
     * Test of enter method, of class RodentsRevengeGame.
     */
    @Test
    public void testEnter() {
        System.out.println("enter");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        instance.enter();
    }

    /**
     * Test of newGame method, of class RodentsRevengeGame.
     */
    @Test
    public void testNewGame() {
        System.out.println("newGame");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        instance.newGame();
    }

    /**
     * Test of turn method, of class RodentsRevengeGame.
     */
    @Test
    public void testTurn() {
        System.out.println("turn");
        Direction d = null;
        RodentsRevengeGame instance = new RodentsRevengeGame();
        instance.turn(d);
    }

    /**
     * Test of startGame method, of class RodentsRevengeGame.
     */
    @Test
    public void testStartGame() {
        System.out.println("startGame");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        instance.startGame();
    }

    /**
     * Test of isNewGame method, of class RodentsRevengeGame.
     */
    @Test
    public void testIsNewGame() {
        System.out.println("isNewGame");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        boolean expResult = false;
        boolean result = instance.isNewGame();
        assertEquals(expResult, result);
    }

    /**
     * Test of hasWon method, of class RodentsRevengeGame.
     */
    @Test
    public void testHasWon() {
        System.out.println("hasWon");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        boolean expResult = false;
        boolean result = instance.hasWon();
        assertEquals(expResult, result);
    }

    /**
     * Test of isGameOver method, of class RodentsRevengeGame.
     */
    @Test
    public void testIsGameOver() {
        System.out.println("isGameOver");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        boolean expResult = false;
        boolean result = instance.isGameOver();
        assertEquals(expResult, result);
    }

    /**
     * Test of isSettingUp method, of class RodentsRevengeGame.
     */
    @Test
    public void testIsSettingUp() {
        System.out.println("isSettingUp");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        boolean expResult = false;
        boolean result = instance.isSettingUp();
        assertEquals(expResult, result);
    }

    /**
     * Test of isPaused method, of class RodentsRevengeGame.
     */
    @Test
    public void testIsPaused() {
        System.out.println("isPaused");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        boolean expResult = false;
        boolean result = instance.isPaused();
        assertEquals(expResult, result);
    }

    /**
     * Test of getScore method, of class RodentsRevengeGame.
     */
    @Test
    public void testGetScore() {
        System.out.println("getScore");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        int expResult = 0;
        int result = instance.getScore();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFruitsEaten method, of class RodentsRevengeGame.
     */
    @Test
    public void testGetFruitsEaten() {
        System.out.println("getFruitsEaten");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        int expResult = 0;
        int result = instance.getFruitsEaten();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDirection method, of class RodentsRevengeGame.
     */
    @Test
    public void testGetDirection() {
        System.out.println("getDirection");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        Direction expResult = null;
        Direction result = instance.getDirection();
        assertEquals(expResult, result);
    }

    /**
     * Test of repeatLevel method, of class RodentsRevengeGame.
     */
    @Test
    public void testRepeatLevel() {
        System.out.println("repeatLevel");
        boolean b = false;
        RodentsRevengeGame instance = new RodentsRevengeGame();
        instance.repeatLevel(b);
    }

    /**
     * Test of getLevel method, of class RodentsRevengeGame.
     */
    @Test
    public void testGetLevel() {
        System.out.println("getLevel");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        int expResult = 0;
        int result = instance.getLevel();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTimeLeft method, of class RodentsRevengeGame.
     */
    @Test
    public void testGetTimeLeft() {
        System.out.println("getTimeLeft");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        int expResult = 0;
        int result = instance.getTimeLeft();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLivesLeft method, of class RodentsRevengeGame.
     */
    @Test
    public void testGetLivesLeft() {
        System.out.println("getLivesLeft");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        int expResult = 0;
        int result = instance.getLivesLeft();
        assertEquals(expResult, result);
    }

    /**
     * Test of getGameClock method, of class RodentsRevengeGame.
     */
    @Test
    public void testGetGameClock() {
        System.out.println("getGameClock");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        GameClock expResult = null;
        GameClock result = instance.getGameClock();
        assertEquals(expResult, result);
    }

    /**
     * Test of isDead method, of class RodentsRevengeGame.
     */
    @Test
    public void testIsDead() {
        System.out.println("isDead");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        boolean expResult = false;
        boolean result = instance.isDead();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMousePosition method, of class RodentsRevengeGame.
     */
    @Test
    public void testGetMousePosition() {
        System.out.println("getMousePosition");
        RodentsRevengeGame instance = new RodentsRevengeGame();
        Point expResult = null;
        Point result = instance.getMousePosition();
        assertEquals(expResult, result);
    }

    /**
     * Test of changeDifficulty method, of class RodentsRevengeGame.
     */
    @Test
    public void testChangeDifficulty() {
        System.out.println("changeDifficulty");
        Difficulty d = null;
        RodentsRevengeGame instance = new RodentsRevengeGame();
        instance.changeDifficulty(d);
    }

    /**
     * Test of setLevel method, of class RodentsRevengeGame.
     */
    @Test
    public void testSetLevel() {
        System.out.println("setLevel");
        int l = 0;
        RodentsRevengeGame instance = new RodentsRevengeGame();
        instance.setLevel(l);
    }
    
}
