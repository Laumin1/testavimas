/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Point;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Waswas
 */
public class LevelTest {
    
    Level instance = new Level(20,20);
    
    public LevelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new Level(20,20);
    }
    
    @After
    public void tearDown() {
        instance = new Level(20,20);
    }

    /**
     * Test of setTimePerRound method, of class Level.
     */
    @Test
    public void testSetTimePerRound() {
        System.out.println("setTimePerRound");
        int minutes = 0;
        instance.setTimePerRound(minutes);
    }

    /**
     * Test of getTimePerRound method, of class Level.
     */
    @Test
    public void testGetTimePerRound() {
        System.out.println("getTimePerRound");
        int expResult = 0;
        int result = instance.getTimePerRound();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumOfWallsToGenerate method, of class Level.
     */
    @Test
    public void testGetNumOfWallsToGenerate() {
        System.out.println("getNumOfWallsToGenerate");
        int expResult = 0;
        int result = instance.getNumOfWallsToGenerate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumOfWallsToGenerate method, of class Level.
     */
    @Test
    public void testSetNumOfWallsToGenerate() {
        System.out.println("setNumOfWallsToGenerate");
        int numOfWallsToGenerate = 0;
        instance.setNumOfWallsToGenerate(numOfWallsToGenerate);
    }

    /**
     * Test of getNumOfMouseTrapsToGenerate method, of class Level.
     */
    @Test
    public void testGetNumOfMouseTrapsToGenerate() {
        System.out.println("getNumOfMouseTrapsToGenerate");
        int expResult = 0;
        int result = instance.getNumOfMouseTrapsToGenerate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumOfMouseTrapsToGenerate method, of class Level.
     */
    @Test
    public void testSetNumOfMouseTrapsToGenerate() {
        System.out.println("setNumOfMouseTrapsToGenerate");
        int numOfMouseTrapsToGenerate = 0;
        instance.setNumOfMouseTrapsToGenerate(numOfMouseTrapsToGenerate);
    }

    /**
     * Test of getNumOfHolesToGenerate method, of class Level.
     */
    @Test
    public void testGetNumOfHolesToGenerate() {
        System.out.println("getNumOfHolesToGenerate");
        int expResult = 0;
        int result = instance.getNumOfHolesToGenerate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumOfHolesToGenerate method, of class Level.
     */
    @Test
    public void testSetNumOfHolesToGenerate() {
        System.out.println("setNumOfHolesToGenerate");
        int numOfHolesToGenerate = 0;
        instance.setNumOfHolesToGenerate(numOfHolesToGenerate);
    }

    /**
     * Test of getNumOfCatsForRound method, of class Level.
     */
    @Test
    public void testGetNumOfCatsForRound() {
        System.out.println("getNumOfCatsForRound");
        int round = 0;
        int expResult = 0;
        int result = instance.getNumOfCatsForRound(round);
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumOfCatsPerRound method, of class Level.
     */
    @Test
    public void testSetNumOfCatsPerRound() {
        System.out.println("setNumOfCatsPerRound");
        int[] numOfCatsPerRound = null;
        instance.setNumOfCatsPerRound(numOfCatsPerRound);
    }

    /**
     * Test of getNumOfBlocksToGenerate method, of class Level.
     */
    @Test
    public void testGetNumOfBlocksToGenerate() {
        System.out.println("getNumOfBlocksToGenerate");
        int expResult = 0;
        int result = instance.getNumOfBlocksToGenerate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumOfBlocksToGenerate method, of class Level.
     */
    @Test
    public void testSetNumOfBlocksToGenerate() {
        System.out.println("setNumOfBlocksToGenerate");
        int numOfBlocksToGenerate = 0;
        instance.setNumOfBlocksToGenerate(numOfBlocksToGenerate);
    }

    /**
     * Test of setMouseLocation method, of class Level.
     */
    @Test
    public void testSetMouseLocation_int_int() {
        System.out.println("setMouseLocation");
        int x = 0;
        int y = 0;
        instance.setMouseLocation(x, y);
    }

    /**
     * Test of setMouseLocation method, of class Level.
     */
    @Test
    public void testSetMouseLocation_Point() {
        System.out.println("setMouseLocation");
        Point p = null;
        instance.setMouseLocation(p);
    }

    /**
     * Test of getMousePosition method, of class Level.
     */
    @Test
    public void testGetMousePosition() {
        System.out.println("getMousePosition");
        Point expResult = null;
        Point result = instance.getMousePosition();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTile method, of class Level.
     */
    @Test
    public void testSetTile_3args() {
        System.out.println("setTile");
        int x = 0;
        int y = 0;
        TileType type = null;
        instance.setTile(x, y, type);
    }

    /**
     * Test of getTile method, of class Level.
     */
    @Test
    public void testGetTile() {
        System.out.println("getTile");
        int x = 0;
        int y = 0;
        TileType expResult = TileType.Wall;
        TileType result = instance.getTile(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRowCount method, of class Level.
     */
    @Test
    public void testGetRowCount() {
        System.out.println("getRowCount");
        int expResult = 0;
        int result = instance.getRowCount();
        assertEquals(expResult, result);
    }

    /**
     * Test of getColCount method, of class Level.
     */
    @Test
    public void testGetColCount() {
        System.out.println("getColCount");
        int expResult = 0;
        int result = instance.getColCount();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBorder method, of class Level.
     */
    @Test
    public void testSetBorder() {
        System.out.println("setBorder");
        instance.setBorder();
    }

    /**
     * Test of getFramesForYarnBallGeneration method, of class Level.
     */
    @Test
    public void testGetFramesForYarnBallGeneration() {
        System.out.println("getFramesForYarnBallGeneration");
        int expResult = 0;
        int result = instance.getFramesForYarnBallGeneration();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFramesForYarnBallGeneration method, of class Level.
     */
    @Test
    public void testSetFramesForYarnBallGeneration() {
        System.out.println("setFramesForYarnBallGeneration");
        int framesForYarnBallGeneration = 0;
        instance.setFramesForYarnBallGeneration(framesForYarnBallGeneration);
    }
    
}
