/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.TreeSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Waswas
 */
public class NodeTest {
    Node instance;
    
    public NodeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new Node(15,15);
    }
    
    @After
    public void tearDown() {
        instance = new Node(15,15);
    }

    /**
     * Test of getCost method, of class Node.
     */
    @Test
    public void testGetCost() {
        System.out.println("getCost");
        double expResult = 0.0;
        double result = instance.getCost();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getX method, of class Node.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        int expResult = 0;
        int result = instance.getX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getY method, of class Node.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        int expResult = 0;
        int result = instance.getY();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTileType method, of class Node.
     */
    @Test
    public void testGetTileType() {
        System.out.println("getTileType");
        TileType expResult = null;
        TileType result = instance.getTileType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTileType method, of class Node.
     */
    @Test
    public void testSetTileType() {
        System.out.println("setTileType");
        TileType t = TileType.Wall;
        instance.setTileType(t);
    }

    /**
     * Test of isVisited method, of class Node.
     */
    @Test
    public void testIsVisited() {
        System.out.println("isVisited");
        boolean expResult = false;
        boolean result = instance.isVisited();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVisited method, of class Node.
     */
    @Test
    public void testSetVisited() {
        System.out.println("setVisited");
        boolean visited = false;
        instance.setVisited(visited);
    }

    /**
     * Test of getPathParent method, of class Node.
     */
    @Test
    public void testGetPathParent() {
        System.out.println("getPathParent");
        Node expResult = null;
        Node result = instance.getPathParent();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPathParent method, of class Node.
     */
    @Test
    public void testSetPathParent() {
        System.out.println("setPathParent");
        Node pathParent = null;
        instance.setPathParent(pathParent);
    }

    /**
     * Test of addNaughbor method, of class Node.
     */
    @Test
    public void testAddNaughbor() {
        System.out.println("addNaughbor");
        Node n = new Node(25,25);
        boolean expResult = false;
        boolean result = instance.addNaughbor(n);
        assertEquals(expResult, result);
    }

    /**
     * Test of compareTo method, of class Node.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Node other = new Node(20,20);
        int expResult = 0;
        int result = instance.compareTo(other);
        assertEquals(expResult, result);
    }

    /**
     * Test of getCostFromStart method, of class Node.
     */
    @Test
    public void testGetCostFromStart() {
        System.out.println("getCostFromStart");
        int expResult = 0;
        int result = instance.getCostFromStart();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCostFromStart method, of class Node.
     */
    @Test
    public void testSetCostFromStart() {
        System.out.println("setCostFromStart");
        int n = 1;
        instance.setCostFromStart(n);
    }

    /**
     * Test of getEstimatedCost method, of class Node.
     */
    @Test
    public void testGetEstimatedCost_0args() {
        System.out.println("getEstimatedCost");
        double expResult = 0.0;
        double result = instance.getEstimatedCost();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setEstimatedCost method, of class Node.
     */
    @Test
    public void testSetEstimatedCost() {
        System.out.println("setEstimatedCost");
        double e = 0.0;
        instance.setEstimatedCost(e);
    }

    /**
     * Test of getNeighbors method, of class Node.
     */
    @Test
    public void testGetNeighbors() {
        System.out.println("getNeighbors");
        TreeSet<Node> expResult = null;
        TreeSet<Node> result = instance.getNeighbors();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Node.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = null;
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }

    /**
     * Test of compare method, of class Node.
     */
    @Test
    public void testCompare() {
        System.out.println("compare");
        Node n1 = new Node(1,1);
        Node n2 = new Node(1,1);
        int expResult = 0;
        int result = instance.compare(n1, n2);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Node.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
