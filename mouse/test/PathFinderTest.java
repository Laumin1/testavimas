/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Point;
import java.util.LinkedList;
import java.util.TreeMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Waswas
 */
public class PathFinderTest {
    
    PathFinder instance;
    private RodentsRevengeGame game;
    
    public PathFinderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new PathFinder();
    }
    
    @After
    public void tearDown() {
        instance = new PathFinder();
    }

    /**
     * Test of getNode method, of class PathFinder.
     */
    @Test
    public void testGetNode_int_int() {
        System.out.println("getNode");
        int x = 15;
        int y = 15;
        Node expResult = new Node(15,15);
        Node result = instance.getNode(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of parseBoard method, of class PathFinder.
     */
    @Test
    public void testParseBoard() {
        System.out.println("parseBoard");
        BoardPanel bp = new BoardPanel(game);
        instance.parseBoard(bp);
    }

    /**
     * Test of findPath method, of class PathFinder.
     */
    @Test
    public void testFindPath() {
        System.out.println("findPath");
        Point start = new Point(0,0);
        Point goal = new Point(1,1);
        LinkedList<Node> expResult = null;
        LinkedList<Node> result = instance.findPath(start, goal);
        assertEquals(expResult, result);
    }

    /**
     * Test of isPath method, of class PathFinder.
     */
    @Test
    public void testIsPath() {
        System.out.println("isPath");
        Point start = new Point(0,0);
        Point goal = new Point(1,1);
        boolean expResult = false;
        boolean result = instance.isPath(start, goal);
        assertEquals(expResult, result);
    }

    /**
     * Test of getboard method, of class PathFinder.
     */
    @Test
    public void testGetboard() {
        System.out.println("getboard");
        TreeMap<Point, Node> expResult = null;
        TreeMap<Point, Node> result = instance.getboard();
        assertEquals(expResult, result);
    }

    /**
     * Test of generateBoard method, of class PathFinder.
     */
    @Test
    public void testGenerateBoard() {
        System.out.println("generateBoard");
        instance.generateBoard();
    }

    /**
     * Test of getNode method, of class PathFinder.
     */
    @Test
    public void testGetNode_Point() {
        System.out.println("getNode");
        Point p = new Point(0,0);
        Node expResult = null;
        Node result = instance.getNode(p);
        assertEquals(expResult, result);
    }
    
}
